# Khemlabs DevTools

Docker image for Khemlabs DevTools on Ubuntu 14.04.

# Install

git clone https://bitbucket.org/khemlabs/docker-devtools.git .

docker build -t khemlabs/devtools .

# Run docker

docker run -d --name devtools -v /space/webapps/:/space/webapps/ khemlabs/devtools

# Connet through SSH

docker inspect -f '{{ .NetworkSettings.IPAddress }}' devtools

ssh root@IPAdress with password 1

# Dependencies

Directory "/space/webapps/"