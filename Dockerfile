############################################################
# Dockerfile to build DevTools Container
# Based on Ubuntu 14.04
############################################################

# Set the base image to Ubuntu
FROM ubuntu:trusty

# File Author / Maintainer
MAINTAINER Maintaner khemlabs

# Install Docker Dev-Tools

# Update the repository
RUN apt-get update

# Install necessary tools
RUN apt-get install -y --no-install-recommends vim wget openssh-server curl htop telnet tcpdump git npm
RUN npm install -g bower

# MAP webapps directory
VOLUME ["/space/webapps/"]

# EXPOSE PORTS
EXPOSE 22

# CHANGE ROOT ACCESS
RUN echo "root:1" | chpasswd
RUN sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config

RUN mkdir /var/run/sshd
ENTRYPOINT ["/usr/sbin/sshd", "-D"]
